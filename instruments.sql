CREATE DATABASE  IF NOT EXISTS `trade_history`;
USE `trade_history`;

DROP TABLE IF EXISTS `deal`;
DROP TABLE IF EXISTS `counterparty`;
DROP TABLE IF EXISTS `instrument`;

CREATE TABLE `instrument` (
  instrument_id int NOT NULL AUTO_INCREMENT,
  instrument_name varchar(45) NOT NULL,
   PRIMARY KEY (`instrument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO instrument (instrument_name)
VALUES
("Astronomica"),("Borealis"),("Celestial"),("Deuteronic"),("Eclipse"),
			("Floral"),("Galactia"),("Heliosphere"),("Interstella"),("Jupiter"),("Koronis"),("Lunatic");

CREATE TABLE `counterparty` (
	counterparty_id int NOT NULL AUTO_INCREMENT,
    counterparty_name varchar(45) NOT NULL,
    PRIMARY KEY(counterparty_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO counterparty (counterparty_name)
VALUES
("Lewis"),("Selvyn"),("Richard"),("Lina"),("John"),("Nidia");   

CREATE TABLE `deal` (
    deal_id int NOT NULL AUTO_INCREMENT,
    instrument_id int NOT NULL,
    counterparty_id int NOT NULL,
    price float NOT NULL,
    deal_type char(1) NOT NULL,
    quantity int NOT NULL,
    deal_date DATE NOT NULL,
    deal_time TIME(0) NOT NULL,
    #CONSTRAINT CH_visits CHECK (number_of_visits > 0),
    PRIMARY KEY(deal_id),
    FOREIGN KEY(instrument_id) REFERENCES instrument(instrument_id),
    FOREIGN KEY(counterparty_id) REFERENCES counterparty(counterparty_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
# use only for mock data: INSERT INTO counterparty (instrument_id, counterparty_id, price, deal_type, quantity, deal_date, deal_time)

